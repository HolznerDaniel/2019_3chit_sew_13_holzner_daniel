﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2019_08._03._DataBinding
{
    class PersonalDetail : INotifyPropertyChanged
    {

        string firstname;
        string lastname;

        public string FirstName
        {
            get { return firstname; }
            set {
                firstname = value;
                OnPropertyChanged("FullName");
                }
        }

        public string LastName
        {
            get { return lastname; }
            set {
                lastname = value;
                OnPropertyChanged("FullName");
                }
        }

        public string FullName
        {
            get { return string.Format("{0} {1}", firstname, lastname); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

    }
}
