﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HelloBoundWorld
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Show();
            DataContext = new TestObject();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {

            BindingExpression expr = Explicit.GetBindingExpression(TextBox.TextProperty);
            expr.UpdateSource();

        }
    }

    public class TestObject : INotifyPropertyChanged
    {

        string text = "Hello World";

        public string Text
        {

            get { return text;}
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }

        }

        private void OnPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }

}
