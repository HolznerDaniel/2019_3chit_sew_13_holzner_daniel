﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Skillmanager
{
    class SkillVM
    {

        private Skill skill;
        public Skill Skill
        { get; set; }

        private Skill slectedskill = null;
        public Skill Selectedskill
        { get; set; }

        ObservableCollection<Skill> Skills = new ObservableCollection<Skill>();

        public SkillVM() { }
        public SkillVM(List<Skill> skills)
        { }

        public void AddSkills(List<Skill> skills)
        {
            skills.Add(new Skill());
        }
        public ICommand AddSkillCommand{ get; set; }
        public void AddSkill() {}
        public ICommand AddPercentageCommand { get; set; }
        public void AddPercentage() { }
        public ICommand RemovePercentageCommand { get; set; }
        public void RemovePercentage() { }


    }
}
