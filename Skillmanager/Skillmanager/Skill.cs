﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skillmanager
{
    class Skill
    {

        private string name;
        private int percent;

        public string Name
        { get; set; }

        public int Percent
        { get; set; }

    }
}
