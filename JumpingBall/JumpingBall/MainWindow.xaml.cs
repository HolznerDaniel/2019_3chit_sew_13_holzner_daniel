﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace JumpingBall
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();

            //Was soll geschehen alle 0,05 Sekunden (Eventhandler) - Funktion zuweisen
            timer.Tick += Physics;

            //Timer soll alle 0.05 Sekunden feuern
            timer.Interval = TimeSpan.FromSeconds(0.05);

        }

        private void radioButtonBlue_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Blue;
            Rectangle.Fill = Brushes.Blue;
        }

        private void radioButtonRed_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Red;
            Rectangle.Fill = Brushes.Red;
        }

        private void radioButtonGreen_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Green;
            Rectangle.Fill = Brushes.Green;
        }

        private void radioButtonOriginal_Click(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Chocolate;
            Rectangle.Fill = Brushes.Bisque;
        }

        private void ButtonStartStop_Click(object sender, RoutedEventArgs e)
        {
            //Gegenteil des aktuellen Zustands
            timer.IsEnabled = !timer.IsEnabled;
        }

        bool GoingRight = true;
        bool GoingDown = true;

        private void Physics(object sender, EventArgs e)
        {
            double speed = 3.0;

            if (CheckBoxFast.IsChecked.Value)
                speed = 10.0;
            MoveBallLeftRight(speed);
            MoveBallUpDown(speed);
        }

        private void MoveBallLeftRight(double speed)
        {
            double x = Canvas.GetLeft(Ball);

            if (GoingRight)
                x += speed;

            else
                x -= speed;

            //wenn rechter Rand erreicht -> umdrehen
            if (x + Ball.Width > TheCanvas.ActualWidth)
            {
                GoingRight = false;
                //nicht über den Rand wandern lassen - sondern auf am Rand setzen
                x = TheCanvas.ActualWidth - Ball.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x < 0.0)
            {
                GoingRight = true;
                //nicht über den Rand wandern lassen - sondern auf 0 (am Rand setzen)
                x = 0.0;
                System.Media.SystemSounds.Beep.Play();
            }
            Canvas.SetLeft(Ball,x);
        }

        private void MoveBallUpDown(double speed)
        {
            double y = Canvas.GetLeft(Ball);

            if (GoingDown)
                y += speed;

            else
                y -= speed;

            //wenn rechter Rand erreicht -> umdrehen
            if (y + Ball.Height > TheCanvas.ActualHeight)
            {
                GoingDown = false;
                //nicht über den Rand wandern lassen - sondern auf am Rand setzen
                y = TheCanvas.ActualHeight - Ball.Height;
                System.Media.SystemSounds.Exclamation.Play();
            }
            else if (y < 0.0)
            {
                GoingDown = true;
                //nicht über den Rand wandern lassen - sondern auf 0 (am Rand setzen)
                y = 0.0;
                System.Media.SystemSounds.Hand.Play();
            }
            Canvas.SetLeft(Ball, y);
        }

        int score = 0;

        private void Ball_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (timer.IsEnabled)
            {
                score++;
                labelScoue.Content = score;
            }
        }

        private void MoveBallRectangleLeftRight(double speed)
        { 
            double x = Canvas.GetLeft(Rectangle);

            if (GoingRight)
                x += speed;

            else
                x -= speed;

            //wenn rechter Rand erreicht -> umdrehen
            if (x + Rectangle.Width > TheCanvas.ActualWidth)
            {
                GoingRight = false;
                //nicht über den Rand wandern lassen - sondern auf am Rand setzen
                x = TheCanvas.ActualWidth - Rectangle.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x < 0.0)
            {
                GoingRight = true;
                //nicht über den Rand wandern lassen - sondern auf 0 (am Rand setzen)
                x = 0.0;
                System.Media.SystemSounds.Beep.Play();
            }
            Canvas.SetLeft(Rectangle, x);
        }

        private void MoveRectangleUpDown(double speed)
        {
            double y = Canvas.GetLeft(Rectangle);

            if (GoingDown)
                y += speed;

            else
                y -= speed;

            //wenn rechter Rand erreicht -> umdrehen
            if (y + Rectangle.Height > TheCanvas.ActualHeight)
            {
                GoingDown = false;
                //nicht über den Rand wandern lassen - sondern auf am Rand setzen
                y = TheCanvas.ActualHeight - Rectangle.Height;
                System.Media.SystemSounds.Exclamation.Play();
            }
            else if (y < 0.0)
            {
                GoingDown = true;
                //nicht über den Rand wandern lassen - sondern auf 0 (am Rand setzen)
                y = 0.0;
                System.Media.SystemSounds.Hand.Play();
            }
            Canvas.SetLeft(Rectangle, y);
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (timer.IsEnabled)
            {
                score++;
                labelScoue.Content = score;
            }
        }

    }
}
