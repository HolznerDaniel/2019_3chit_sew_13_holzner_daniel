﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class Program
    {
        static void Main(string[] args)
        {

            PancakeHouseMenu pancakeHouseMenu = new PancakeHouseMenu();
            List<object> breakfastItems = pancakeHouseMenu.getMenuItems();

            DinerMenu dinerMenu = new DinerMenu();
            MenuItem[] lunchItems = dinerMenu.getMenuItems();

            for (int i = 0; i < breakfastItems.Count; i++)
            {
                MenuItem menuItem = breakfastItems.Find(i);
                Console.WriteLine(menuItem.getName() + " ");
                Console.WriteLine(menuItem.getPrice() + " ");
                Console.WriteLine(menuItem.getDescription());
            }

            for (int i = 0; i < lunchItems.Length; i++)
            {
                MenuItem menuItem = lunchItems[i];
                Console.WriteLine(menuItem.getName() + " ");
                Console.WriteLine(menuItem.getPrice() + " ");
                Console.WriteLine(menuItem.getDescription());
            }

        }
    }
}
