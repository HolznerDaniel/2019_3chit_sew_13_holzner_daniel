﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Employees
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Show();
        }
    }

    public class Employee
    {
        public int MemberID { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Salary { get; set; }
    }

    public class MainViewModel
    {
        //private ObservableCollection<Employee> _employees;
        //public ObservableCollection<Employee> Employees;
        public ObservableCollection<Employee> Employees { get; set; }
        public MainViewModel()
        {
            InitializeList();
        }
        public void Initialize()
        {
            Employees = new ObservableCollection<Employee>();
            Employee employee = new Employee()
            {
                MemberID = 6,
                Name = "Jane Doe",
                Department = "Banking",
                Phone = "31234748",
                Email = "Jane.Doe@Company.Com",
                Salary = "3350"
            };
            Employees.Add(employee);
        }
        public void InitializeList()
        {
            Employees = new ObservableCollection<Employee>();
            Employees.Add(new Employee { MemberID = 1, Name = "John Hancock", Department = "IT", Phone = "31234743", Email = @"John.Hancock@Company.com", Salary = "3450.44" });
            Employees.Add(new Employee { MemberID = 2, Name = "Jane Hayes", Department = "Sales", Phone = "31234744", Email = @"Jane.Hayes@Company.com", Salary = "3700" });
            Employees.Add(new Employee { MemberID = 3, Name = "Larry Jones", Department = "Marketing", Phone = "31234745", Email = @"Larry.Jones@Company.com", Salary = "3000" });
            Employees.Add(new Employee { MemberID = 4, Name = "Patricia Palce", Department = "Secretary", Phone = "31234746", Email = @"Patricia.Palce@Company.com", Salary = "2900" });
            Employees.Add(new Employee { MemberID = 5, Name = "Jean L. Trickard", Department = "Director", Phone = "31234747", Email = @"Jean.L.Tricard@Company.com", Salary = "5400" });
        }
    }


}
